#include <GUIConstantsEx.au3>
#include <MsgBoxConstants.au3>
#include <StringConstants.au3>
#include <TrayConstants.au3> ; Required for the $TRAY_ICONSTATE_SHOW constant.
#include <File.au3>

#include<ComboConstants.au3>


#include <Array.au3>

#include <GuiComboBox.au3>

#include <String.au3>
#include <WindowsConstants.au3>
#include <Array.au3>
#include <Process.au3>
#include "Toast.au3"
#include <CoProcEx.au3>



Global $snip_inifile = @ScriptDir & "\" & "config.ini"
Global $processIdMain , $processIdIcon
;If $HKSTRING is changed, make sure to adjust the $z number in the SetHotkeyKeys function.
Local $HKSTRING = "[;][#;Win][!;Alt][+;Shift][^;Ctrl][0;0][1;1][2;2][3;3][4;4][5;5][6;6][7;7][8;8][9;9][a;a][b;b][c;c][d;d][e;e][f;f][g;g][h;h][i;i][j;j][k;k][l;l][m;m][n;n][o;o][p;p][q;q][r;r][s;s][t;t][u;u][v;v][w;w][x;x][y;y][z;z][{PAUSE};Pause][{BREAK};Break][{HOME};Home/Pos1][{END};END][{PGUP};Page UP][{PGDN};Page Down][{F1};F1][{F2};F2][{F3};F3][{F4};F4][{F5};F5][{F6};F6][{F7};F7][{F8};F8][{F9};F9][{F10};F10][{F11};F11][{F12};F12][{SPACE};Space]"
Local $HKT = _StringBetween($HKSTRING, "[", "]")
Global $HK, $HKS
Global $h_HotkeyTestInput
Dim $HK[0][2]                            ;Hotkey keys+names
Dim $HKS[6]                              ;Hotkey (ini)settings 0-3 combo boxes, 4 = Redirect the hotkey test to the input box, 5=Current Hotkey Set.

For $x = 0 To UBound($HKT) - 1            ;Make an array including the text from HKSTRING
    _ArrayAdd($HK, $HKT[$x], 0, ";")
Next

$HKT = ""
$HKSTRING = ""



Func SetHotkeyKeys($showgui = 0)
    ;$showgui 0 = Set the saved hotkeys, 1 = Show gui to change the settings
    Local $a, $b, $c, $z
    $z=4                                                ;$z of the text from $HK will go in to c1-c3, the rest goes to c4
    $HKS[0] = IniRead($snip_inifile, "hotkey", "0", "4")
    $HKS[1] = IniRead($snip_inifile, "hotkey", "1", "0")
    $HKS[2] = IniRead($snip_inifile, "hotkey", "2", "0")
    $HKS[3] = IniRead($snip_inifile, "hotkey", "3", "12")
    $HKS[4] = 0
    $HKS[5] = $HK[$HKS[0]][0] & $HK[$HKS[1]][0] & $HK[$HKS[2]][0] & $HK[$HKS[3] + $z + 1][0]
    If $showgui = 1 Then
        $HKS[4] = 1
        #Region ### START Koda GUI section ### Form=
        $Form1 = GUICreate("Hotkey Config", 317, 79, -1, -1, $WS_POPUP + $WS_CAPTION)
        $C1 = GUICtrlCreateCombo("", 4, 50, 72, 25, BitOR($CBS_DROPDOWNLIST, $CBS_AUTOHSCROLL))
        $C2 = GUICtrlCreateCombo("", 82, 50, 72, 25, BitOR($CBS_DROPDOWNLIST, $CBS_AUTOHSCROLL))
        $C3 = GUICtrlCreateCombo("", 161, 50, 72, 25, BitOR($CBS_DROPDOWNLIST, $CBS_AUTOHSCROLL))
        $C4 = GUICtrlCreateCombo("", 239, 50, 72, 25, BitOR($CBS_DROPDOWNLIST, $CBS_AUTOHSCROLL))
        $Label1 = GUICtrlCreateLabel("Please set a hotkey to use. Note: Not all combos may work.", 4, 30, 300, 17)
        $Button1 = GUICtrlCreateButton("Set + Save", 4, 4, 70, 23)
        $h_HotkeyTestInput = GUICtrlCreateInput("Nhờ đồng chí nhấn test thử !", 82, 5, 150, 21)
        $Button2 = GUICtrlCreateButton("Exit", 239, 5, 72, 23)
        _GUICtrlComboBox_SetMinVisible($C4, 60)
        GUISetState(@SW_SHOW)
        #EndRegion ### END Koda GUI section ###

        For $x = 0 To UBound($HK) - 1
            If $x <= $z Then
                _GUICtrlComboBox_AddString($C1, $HK[$x][1])
                _GUICtrlComboBox_AddString($C2, $HK[$x][1])
                _GUICtrlComboBox_AddString($C3, $HK[$x][1])
            Else
                _GUICtrlComboBox_AddString($C4, $HK[$x][1])
            EndIf
        Next
        _GUICtrlComboBox_SetCurSel($C1, $HKS[0])
        _GUICtrlComboBox_SetCurSel($C2, $HKS[1])
        _GUICtrlComboBox_SetCurSel($C3, $HKS[2])
        _GUICtrlComboBox_SetCurSel($C4, $HKS[3])

        While 1
            $nMsg = GUIGetMsg()
            Switch $nMsg
                Case $GUI_EVENT_CLOSE, $Button2
                    GUIDelete($Form1)
                    ExitLoop
                Case $Button1
                    $HKS[0] = _GUICtrlComboBox_GetCurSel($C1)       ;Read the combo - boxes
                    $HKS[1] = _GUICtrlComboBox_GetCurSel($C2)
                    $HKS[2] = _GUICtrlComboBox_GetCurSel($C3)
                    $HKS[3] = _GUICtrlComboBox_GetCurSel($C4)
                    HotKeySet($HKS[5])                              ;Clear the Old Hotkey
                    $HKS[5] = $HK[$HKS[0]][0] & $HK[$HKS[1]][0] & $HK[$HKS[2]][0] & $HK[$HKS[3] + $z +1][0]     ;Assemble the hotkey string, use $z+1 !
                    If HotKeySet($HKS[5], "DoHotkey") = 0 Then MsgBox(0, "Hotkey", "Err: Could not set. " & $HKS[5])        ;Set the hotkey or display a message
                    IniWrite($snip_inifile, "hotkey", "0", $HKS[0])                 ;Save the hotkey to the ini file
                    IniWrite($snip_inifile, "hotkey", "1", $HKS[1])
                    IniWrite($snip_inifile, "hotkey", "2", $HKS[2])
                    IniWrite($snip_inifile, "hotkey", "3", $HKS[3])
            EndSwitch
        WEnd
    EndIf

    If $HKS[4] = 0 Then                     ;Function was called with 0
        $HKS[5] = $HK[$HKS[0]][0] & $HK[$HKS[1]][0] & $HK[$HKS[2]][0] & $HK[$HKS[3] + $z + 1][0]
        If HotKeySet($HKS[5], "DoHotkey") = 0 Then MsgBox(0, "Hotkey", "Err: Could not set. " & $HKS[5])
    Else
        $HKS[4] = 0
    EndIf
EndFunc   ;==>SetHotkeyKeys

Func DoHotkey()

    If $HKS[4] = 0 Then                 ;Main function
		;
        ;ConsoleWrite("PRESS" & @CRLF)
		$vpn_name = IniRead($snip_inifile, "VPN", "name", "error_vpn");
		If $vpn_name = "error_vpn" Then
			MsgBox(0, "error", "Cannot find VPN name on config !")
		Else
			; connect vpn
			Local $isNoConnect = checkVpnConnected()
			If $isNoConnect = 0 Then
				; is connected
				; then disconnect
				Local $isDis = disConnectVPN()
				If $isDis = 1 Then
						; show toast
					_Toast_Set(5, 0xFF00FF, 0xFFFF00, 0xff0000, 0xFFFFFF, 10, "Arial")
					_Toast_Show(0, "", IniRead($snip_inifile, "VPN", "name", "") & " VPN disconnected", 1.5)
					_Toast_Hide()
				EndIf
			Else
				; connect VPN
				Local $isConnect = connectVPN()
				If $isConnect = 0 Then  ; = 0 is connected
					; show toast
					_Toast_Set(5, 0xFF00FF, 0xFFFF00, 0x009933, 0xFFFFFF, 10, "Arial")
					_Toast_Show(0, "", IniRead($snip_inifile, "VPN", "name","") & "VPN connected", 1.5)
					_Toast_Hide()
				EndIf

			EndIf
		EndIf
    Else                                ;Gui test code:
        GUICtrlSetData($h_HotkeyTestInput, "Hotkey ! " & @hour & ":" & @min & ":" & @SEC)
    EndIf
EndFunc   ;==>DisplayActiveProcess


Func checkVpnConnected()
	$sOutput = runCommandLine("rasdial")
	; return 1 is NO connect
	Return StringRegExp($sOutput, "No connections")
EndFunc

Func disConnectVPN()
	$sOutput = runCommandLine("rasdial /d")
	; return 1 is completed
	Return StringRegExp($sOutput, "completed")
EndFunc

Func connectVPN()
	Local $vpnName = IniRead($snip_inifile, "VPN", "name", "error")
	If $vpnName = "error" Then
		Return
	EndIf
	runCommandLineNotOutput("rasphone -d " & $vpnName)
	; return 1 is completed
	Return checkVpnConnected()
EndFunc

Func runCommandLine($sCmd)
	$fileOutput = @ScriptDir & "\" & _Randomstring(10) & ".txt"
	RunWait(@ComSpec & " /c " & '"' & $sCmd & " > " & $fileOutput & '"', "", @SW_HIDE)
	$sOutput = readFile($fileOutput)
	FileDelete($fileOutput)
	Return $sOutput
EndFunc

Func runCommandLineNotOutput($sCmd)
	;$fileOutput = @ScriptDir & "\" & _Randomstring(10) & ".txt"
	RunWait(@ComSpec & " /c " & '"' & $sCmd & '"', "", @SW_HIDE)
	;$sOutput = readFile($fileOutput)
	;FileDelete($fileOutput)
	;Return $sOutput
EndFunc

Func readFile($fileName)
	Local $hFileOpen = FileOpen($fileName, $FO_READ)
	If $hFileOpen = -1 Then
		MsgBox($MB_SYSTEMMODAL, "", "An error occurred when reading the file.")
		Return False
	EndIf
	; Read the contents of the file using the handle returned by FileOpen.
	Local $sFileRead = FileRead($hFileOpen)
	; Close the handle returned by FileOpen.
	FileClose($hFileOpen)
	Return $sFileRead
EndFunc


Func _Randomstring($length)
    $chars= StringSplit("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789","")
    $String = ""
    $i=0
    Do
        If $length<=0 then ExitLoop
        $String &=  $chars[Random(1,$chars[0])]
        $i += 1
    Until $i = $length
    Return $String
EndFunc







Opt("TrayMenuMode", 3) ; The default tray menu items will not be shown and items are not checked when selected. These are options 1 and 2 for TrayMenuMode.
MainFnc()


Func updateTrayIcon()
	Local $isConneced = checkVpnConnected()
	If $isConneced = 0 Then
		TraySetIcon("shell32.dll", 16802)
	Else
		TraySetIcon("shell32.dll", 200)
	EndIf
EndFunc

Func realTimeUpdateTrayIcon($processIDMain)
	Opt("TrayMenuMode", 3)
	While Sleep(1000)
		if ProcessExists($processIDMain) = 0 Then
			ExitLoop
		EndIf
		updateTrayIcon()
	WEnd
EndFunc
Func MainFnc()

	; tray icon
	Local $idSetting = TrayCreateItem("Setting")
    TrayCreateItem("") ; Create a separator line.
	$processIdMain = @AutoItPID
    Local $idExit = TrayCreateItem("Exit")
    TraySetState($TRAY_ICONSTATE_SHOW) ; Show the tray menu.
	TraySetIcon("shell32.dll", 42)
    ; Create a GUI with various controls.
    Local $hGUI = GUICreate("HuyTeamTools", 220, 150)
	GUISetIcon("shell32.dll", 42)
    Local $idHidden = GUICtrlCreateButton("Run background", 100, 115, 100, 25)

	;  tab
	GUICtrlCreateTab(10, 10, 200, 100)
	GUICtrlCreateTabItem("VPN")

	; set non preview username passs
	setPreviewNonVPN()

	Local $idComboBoxVPN = GUICtrlCreateCombo("", 20, 80, 185, 20, $CBS_DROPDOWNLIST)
	$listVPN = getListVPN();
	For $i=0 to UBound($listVPN) -1
		; Add additional items to the combobox.
		GUICtrlSetData($idComboBoxVPN, $listVPN[$i], IniRead($snip_inifile, "VPN", "name", ""))
	Next

    Local $idBtnHotKey = GUICtrlCreateButton("Set Hot Key", 20, 50, 150, 20)


	GUICtrlCreateTab(10, 10, 200, 100)
	GUICtrlCreateTabItem("JAV collection")

	GUICtrlCreateLabel("Chào đồng chí ! Giờ làm việc nên khum được xem JAV", 12, 40, 200, 100)




    ; Display the GUI.
    GUISetState(@SW_SHOW, $hGUI)

	;hot key
	SetHotkeyKeys(0)

	;check vpn thread
	_CoProc_Create("realTimeUpdateTrayIcon", $processIdMain)
	While 1
		$msg = GUIGetMsg()
		$tmsg = TrayGetMsg()
		Select
			Case $tmsg = $idSetting
				GUISetState(@SW_SHOW, $hGUI)
			Case $tmsg = $idExit
				ExitLoop
			Case $msg = $idHidden
				GUISetState(@SW_HIDE, $hGUI)
			Case $msg = $idBtnHotKey
				SetHotkeyKeys(1)
			Case $msg = $idComboBoxVPN
				IniWrite($snip_inifile, "VPN", "name", GUICtrlRead($idComboBoxVPN))
			Case $msg = $GUI_EVENT_CLOSE
				GUISetState(@SW_HIDE, $hGUI)
		EndSelect
	WEnd



    ; Delete the previous GUI and all controls.
    GUIDelete($hGUI)
EndFunc   ;==>Example


Func ShowMessage()
	MsgBox($MB_SYSTEMMODAL, "", "This is a message.")
EndFunc   ;==>ShowMessage

Func getListVPN()
		; file vpn
		Local $filePathRasphone
		$filePathRasphone = _PathFull("Microsoft\Network\Connections\Pbk\rasphone.pbk", @AppDataDir)
		Local $hFileOpen = FileOpen($filePathRasphone, $FO_READ)
		If $hFileOpen = -1 Then
			MsgBox($MB_SYSTEMMODAL, "", "An error occurred when reading the file.")
			Return False
		EndIf
		; Read the contents of the file using the handle returned by FileOpen.
		Local $sFileRead = FileRead($hFileOpen)

		; Close the handle returned by FileOpen.
		FileClose($hFileOpen)

		; Display the contents of the file.
		$a = StringRegExp($sFileRead, "(?<=\[).*(?=\])", 3 )
		;MsgBox($MB_SYSTEMMODAL, "", "Contents of the file:" & UBound($a))

		return $a
EndFunc

Func setPreviewNonVPN()
		; file vpn
		Local $filePathRasphone
		$filePathRasphone = _PathFull("Microsoft\Network\Connections\Pbk\rasphone.pbk", @AppDataDir)
		Local $hFileOpen = FileOpen($filePathRasphone, $FO_READ)
		If $hFileOpen = -1 Then
			MsgBox($MB_SYSTEMMODAL, "", "An error occurred when reading the file.")
			Return False
		EndIf
		; Read the contents of the file using the handle returned by FileOpen.
		Local $sFileRead = FileRead($hFileOpen)


		; Close the handle returned by FileOpen.
		FileClose($hFileOpen)


		; Display the contents of the file.
		;MsgBox($MB_SYSTEMMODAL, "", "Contents of the file:" & UBound($a))

		;replace
		$content = StringRegExpReplace($sFileRead, "PreviewUserPw=1", "PreviewUserPw=0")


		; write file
		Local $hFileOpen = FileOpen($filePathRasphone, $FO_OVERWRITE)
		FileWrite($hFileOpen, $content)

		; Close the handle returned by FileOpen.
		FileClose($hFileOpen)

EndFunc
